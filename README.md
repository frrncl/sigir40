SIGIR 40th Anniversary
==========================

This repository contains the source code developed for analysing 40 years of SIGIR
publications.

Main developers:

  * Nicola Ferro (`ferro@dei.unipd.it`, [homepage](http://www.dei.unipd.it/~ferro/))
    
    
Credits:

  * Djoerd Hiemstra (`hiemstra@cs.utwente.nl`, [homepage](http://wwwhome.ewi.utwente.nl/~hiemstra/))
    extracted information about SIGIR publications from [DBLP](http://dblp.uni-trier.de/db/conf/sigir/)
    and made them available in a cleaned XML format.