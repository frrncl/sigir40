/*
 *  Copyright 2017 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.ims.sigir40;

import java.util.Map;
import java.util.TreeMap;

/**
 * Represents statistics about an author.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
class AuthorStats implements Comparable<AuthorStats> {

    /**
     * The unique name of an author
     */
    final String name;

    /**
     * The total number of papers published by an author
     */
    int papers = 0;

    /**
     * The total number of full papers published by an author
     */
    int fullPapers = 0;

    /**
     * The total number of short papers papers published by an author
     */
    int shortPapers = 0;

    /**
     * The total number of pages written by an author
     */
    int pages = 0;

    /**
     * Information about all the co-authors of a given author
     */
    final Map<String, CoauthorStats> coauthors = new TreeMap<>();

    /**
     * The year-by-year information about co-authors
     */
    final Map<Integer, Map<String, CoauthorStats>> yearlyCoauthors = new TreeMap<>();


    /**
     * Creates a new object for keeping statistics about an author.
     *
     * @param name the unique name of the author.
     */
    public AuthorStats(final String name) {
        this.name = name;
    }

    @Override
    public boolean equals(final Object o) {
        return (this == o) || (o instanceof AuthorStats && name.equals(((AuthorStats) o).name));
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public int compareTo(AuthorStats o) {
        return o == null ? 1 : name.compareTo(o.name);
    }

}
