/*
 *  Copyright 2017 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.ims.sigir40;

/**
 * Represents information about a co-author.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
class CoauthorInfo {

    /**
     * The unique name of the author
     */
    final String author;

    /**
     * The unique name of the co-author
     */
    final String coauthor;

    /**
     * The total number of papers published together
     */
    int papers = 0;

    /**
     * The total number of full papers published together
     */
    int fullPapers = 0;

    /**
     * The total number of short papers papers published together
     */
    int shortPapers = 0;

    /**
     * The total number of pages written together
     */
    int pages = 0;

    /**
     * Creates a new object for keeping information about a co-author.
     *
     * @param author   the unique name of the author.
     * @param coauthor the unique name of the coauthor.
     */
    public CoauthorInfo(final String author, final String coauthor) {
        this.author = author;
        this.coauthor = coauthor;
    }

    @Override
    public boolean equals(final Object o) {
        return (this == o) || (o instanceof CoauthorInfo && coauthor.equals(((CoauthorInfo) o).coauthor));
    }

    @Override
    public int hashCode() {
        return coauthor.hashCode();
    }

}
