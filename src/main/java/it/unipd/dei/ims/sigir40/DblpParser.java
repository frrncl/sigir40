/*
 *  Copyright 2017 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.ims.sigir40;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.ParameterizedMessage;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Parses a DBLP dump and computes some statistics and bibliometrics.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class DblpParser {

    /**
     * The logger
     */
    private static final Logger LOGGER = LogManager.getLogger(DblpParser.class);

    /**
     * The file name of the default DBLP file to be parsed.
     */
    private static final String DEFAULT_DBLP_FILE = "sigir-v2.xml";

    /**
     * The XML input factory.
     */
    private static final XMLInputFactory XIF;

    /**
     * The name of the element containing a paper
     */
    private static final String INPROCEEDINGS_ELEMENT = "inproceedings";

    /**
     * The name of the attribute containing the type of a paper
     */
    private static final String TYPE_ATTRIBUTE = "type";

    /**
     * The name of the attribute containing the DBLP key of a paper
     */
    private static final String KEY_ATTRIBUTE = "key";

    /**
     * The value {@code fullpaper} for the {@code TYPE_ATTRIBUTE}.
     */
    private static final String TYPE_ATTRIBUTE_FULLPAPER_VALUE = "fullpaper";

    /**
     * The value {@code shortpaper} for the {@code TYPE_ATTRIBUTE}.
     */
    private static final String TYPE_ATTRIBUTE_SHORTPAPER_VALUE = "shortpaper";

    /**
     * The name of the element containing the identifier of a paper
     */
    private static final String EE_ELEMENT = "ee";

    /**
     * The name of the element containing the title of a paper
     */
    private static final String TITLE_ELEMENT = "title";

    /**
     * The name of the element containing the author of a paper
     */
    private static final String AUTHOR_ELEMENT = "author";

    /**
     * The name of the element containing the author of a paper
     */
    private static final String YEAR_ELEMENT = "year";

    /**
     * The name of the element containing the pages of a paper
     */
    private static final String PAGES_ELEMENT = "pages";

    /**
     * The estimated total number of authors for pre-sizing data structures
     */
    private static final int ESTIMATED_AUTHORS = 7000;

    /**
     * The estimated number of authors per paper for pre-sizing data structures
     */
    private static final int ESTIMATED_PAPER_AUTHORS = 25;


    /**
     * The input stream to be used to parse the DBLP file.
     */
    private final InputStream in;

    /**
     * The XML stream reader for parsing the document.
     */
    private final XMLStreamReader xsr;

    /**
     * Keys are authors and values are statistics about them.
     */
    private final Map<String, AuthorStats> authors;

    /**
     * The parsed papers
     */
    private final Set<Record> papers;

    /**
     * The total number of published full papers
     */
    private int fullPaperCount = 0;

    /**
     * The total number of published short papers
     */
    private int shortPaperCount = 0;

    /**
     * The paper with the longest title
     */
    private Record longestTitle = null;

    /**
     * The paper with the shortest title
     */
    private Record shortestTitle = null;

    /**
     * The paper with the maximum number of authors
     */
    private Record maxAuthors = null;

    static {
        // the StAX XML input factory
        XIF = XMLInputFactory.newInstance();

        XIF.setProperty(XMLInputFactory.SUPPORT_DTD, Boolean.TRUE);
        XIF.setProperty(XMLInputFactory.IS_VALIDATING, Boolean.FALSE);
        XIF.setProperty(XMLInputFactory.IS_COALESCING, Boolean.TRUE);
        XIF.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, Boolean.FALSE);
        XIF.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, Boolean.TRUE);
        XIF.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, Boolean.FALSE);

        XIF.setXMLReporter((message, errorType, relatedInformation, location) -> LOGGER
                .error("Error {} at line {}, column {}: {}.", errorType, location.getLineNumber(),
                       location.getColumnNumber(), message));


        LOGGER.info("StAX XML input factory factory successfully instantiated.");
    }


    /**
     * Creates a new DBLP document parser.
     *
     * @param fileName the name of the DBLP file to be parsed.
     *
     * @throws IllegalStateException if any error occurs while creating the parser.
     */
    public DblpParser(String fileName) {

        if (fileName == null) {
            fileName = DEFAULT_DBLP_FILE;
        }


        // Get the class loader
        ClassLoader cl = DblpParser.class.getClassLoader();
        if (cl == null) {
            cl = ClassLoader.getSystemClassLoader();
            LOGGER.debug("Using system class loader.");
        }

        // Get the stream for reading the DBLP file
        in = cl.getResourceAsStream(fileName);
        if (in == null) {
            final Message msg = new ParameterizedMessage("The DBLP file {} cannot be opened.", fileName);
            LOGGER.error(msg);
            throw new IllegalStateException(msg.getFormattedMessage());
        }


        try {
            xsr = XIF.createXMLStreamReader(in);
        } catch (final XMLStreamException e) {
            LOGGER.error("Unable to instantiate the DBLP document parser.", e);

            try {
                in.close();
            } catch (IOException ee) {
                LOGGER.error("Unable to close the reader.", ee);
            }

            throw new IllegalStateException("Unable to instantiate the DBLP document parser.", e);
        }

        authors = new TreeMap<>();

        papers = new TreeSet<>();
    }

    /**
     * Creates a new DBLP document parser.
     *
     * @throws IllegalStateException if any error occurs while creating the parser.
     */
    public DblpParser() {
        this(null);
    }


    /**
     * Parses the DBLP document.
     *
     * @throws IllegalStateException if any error occurs while creating the parser.
     */
    public final void parse() {

        // keys are authors, values are authors, this is just to ensure we always use the same string
        final Map<String, String> names = new HashMap<>(ESTIMATED_AUTHORS);

        // the list of the authors for the current paper
        final Set<String> currentAuthors = new HashSet<>(ESTIMATED_PAPER_AUTHORS);

        // the publication year of the current paper
        int year = 0;

        // the number of pages of a paper
        int length = 0;

        // the title of a paper
        String title = null;

        // the identifier of a paper
        String id = null;

        // the DBLP key of a paper
        String key = null;

        // the name of a author of a paper
        String name = null;

        // the papersCount of a paper
        String pages = null;

        // the start page of a paper
        int startPage = 0;

        // the end page of a paper
        int endPage = 0;

        // index of the page separator
        int idx = 0;

        // indicates whether it is a full paper or not
        boolean fullPaper = false;

        // the currently parsed paper
        Record paper = null;

        // the co-authors of the current author
        Map<String, CoauthorStats> coauthors = null;

        // statistics about the current co-author
        CoauthorStats coauthorStats = null;


        try {

            // while there are still papersCount to read
            while (alignAtElementStart(INPROCEEDINGS_ELEMENT)) {

                fullPaper = TYPE_ATTRIBUTE_FULLPAPER_VALUE.equals(xsr.getAttributeValue(null, TYPE_ATTRIBUTE));

                key = xsr.getAttributeValue(null, KEY_ATTRIBUTE);

                // parse until we reach the end of the inproceedings element
                while (!(xsr.next() == XMLStreamConstants.END_ELEMENT && INPROCEEDINGS_ELEMENT.equals(
                        xsr.getLocalName()))) {


                    if (xsr.getEventType() == XMLStreamConstants.START_ELEMENT) {

                        switch (xsr.getLocalName()) {

                            case EE_ELEMENT:

                                id = xsr.getElementText();

                                break;

                            case TITLE_ELEMENT:

                                title = xsr.getElementText();

                                break;

                            case YEAR_ELEMENT:

                                year = Integer.parseInt(xsr.getElementText());

                                break;

                            case AUTHOR_ELEMENT:

                                name = xsr.getElementText();

                                // try to get an existing Java String Object for this author name
                                name = names.getOrDefault(name, name);

                                // add the current author name, if not already present
                                names.putIfAbsent(name, name);

                                if (currentAuthors.contains(name)) {
                                    Message msg = new ParameterizedMessage(
                                            "Paper {} contains author {} more than once.", key, name);
                                    LOGGER.warn(msg);
                                }

                                // add the author to the list of the authors of the current paper
                                currentAuthors.add(name);

                                break;

                            case PAGES_ELEMENT:

                                pages = xsr.getElementText();

                                idx = pages.indexOf('-');

                                if (idx > 0) {
                                    startPage = Integer.parseInt(pages.substring(0, idx));
                                    endPage = Integer.parseInt(pages.substring(idx + 1));
                                } else {
                                    startPage = Integer.parseInt(pages);
                                    endPage = startPage;
                                }

                                length = endPage - startPage + 1;

                                break;
                        }


                    }

                }

                if (currentAuthors.size() == 0) {
                    Message msg = new ParameterizedMessage("Paper {} has no authors.", key);
                    LOGGER.error(msg);
                }

                paper = new Record(key, id, title, year, pages, length, fullPaper);
                paper.authors.addAll(currentAuthors);

                if (papers.contains(paper)) {
                    Message msg = new ParameterizedMessage("Paper {} already found.", key);
                    LOGGER.error(msg);
                    throw new IllegalStateException(msg.getFormattedMessage());
                }

                papers.add(paper);

                // increment the number of full/short papers
                if (fullPaper) {
                    fullPaperCount++;
                } else {
                    shortPaperCount++;
                }


                // find the paper with the longest title
                longestTitle = longestTitle == null ? paper :
                        (longestTitle.title.length() > paper.title.length() ? longestTitle : paper);

                // find the paper with the shortest title
                shortestTitle = shortestTitle == null ? paper :
                        (shortestTitle.title.length() < paper.title.length() ? shortestTitle : paper);

                // find the paper with the maximum number of authors
                maxAuthors = maxAuthors == null ? paper :
                        (maxAuthors.authors.size() > paper.authors.size() ? maxAuthors : paper);


                for (String author : currentAuthors) {

                    AuthorStats authorStats = authors.getOrDefault(author, new AuthorStats((author)));

                    // add the stats of the author, if not already present
                    authors.putIfAbsent(author, authorStats);

                    // increment the total number of papers published by by the author
                    authorStats.papers++;

                    // increment the number of full/short papers published by by the author
                    if (fullPaper) {
                        authorStats.fullPapers++;
                    } else {
                        authorStats.shortPapers++;
                    }

                    // increment the number of pages written by the author
                    authorStats.pages += length;

                    // get the yearly statistics about the co-authors or create a new one
                    coauthors = authorStats.yearlyCoauthors.getOrDefault(year, new TreeMap<>());

                    // add the yearly statistics about the co-authors, if not already present
                    authorStats.yearlyCoauthors.putIfAbsent(year, coauthors);


                    // process co-authors
                    for (String coauthor : currentAuthors) {

                        // avoid processing the author itself
                        if (!author.equals(coauthor)) {

                            // get the overall statistics about the co-author or create a new one
                            coauthorStats = authorStats.coauthors.getOrDefault(coauthor,
                                                                               new CoauthorStats(author, coauthor));

                            // add the statistics about the co-author, if not already present
                            authorStats.coauthors.putIfAbsent(coauthor, coauthorStats);

                            // increment the total number of papers written together
                            coauthorStats.papers++;

                            // increment the number of full/short papers written together
                            if (fullPaper) {
                                coauthorStats.fullPapers++;
                            } else {
                                coauthorStats.shortPapers++;
                            }

                            // increment the number of pages written together
                            coauthorStats.pages += length;


                            // get the years statistics about the co-author or create a new one
                            coauthorStats = coauthors.getOrDefault(coauthor, new CoauthorStats(author, coauthor));

                            // add the yearly statistics about the co-author, if not already present
                            coauthors.putIfAbsent(coauthor, coauthorStats);

                            // increment the total number of papers written together
                            coauthorStats.papers++;

                            // increment the number of full/short papers written together
                            if (fullPaper) {
                                coauthorStats.fullPapers++;
                            } else {
                                coauthorStats.shortPapers++;
                            }

                            // increment the number of pages written together
                            coauthorStats.pages += length;


                        }


                    }

                }

                currentAuthors.clear();

            }


        } catch (final XMLStreamException e) {
            LOGGER.error("Unable to parse the DBLP document.", e);
            throw new IllegalStateException("Unable to parse the DBLP document.", e);
        } finally {

            try {
                xsr.close();
            } catch (XMLStreamException e) {
                LOGGER.error("Unable to close the XML document parser.", e);
                throw new IllegalStateException("Unable to close the XML document parser.", e);
            } finally {

                try {
                    in.close();
                } catch (IOException e) {
                    LOGGER.error("Unable to close the DBLP file.", e);
                    throw new IllegalStateException("Unable to close the DBLP file.", e);
                }


            }
        }

    }

    /**
     * Print statistics about the DBLP document.
     */
    public void printStats() {

        System.out.printf(Locale.ENGLISH, "%n");
        System.out.printf(Locale.ENGLISH, "+ Total number of papers: %,d%n", papers.size());
        System.out.printf(Locale.ENGLISH, "  - total number of full papers: %,d%n", fullPaperCount);
        System.out.printf(Locale.ENGLISH, "  - total number of short papers: %,d%n", shortPaperCount);
        System.out.printf(Locale.ENGLISH, "%n");

        System.out.printf(Locale.ENGLISH, "+ Total number of authors: %,d%n", authors.size());
        System.out.printf(Locale.ENGLISH, "%n");

        final IntSummaryStatistics coauthorsSummary = authors.values().stream().mapToInt(s -> s.coauthors.size())
                                                             .collect(IntSummaryStatistics::new,
                                                                      IntSummaryStatistics::accept,
                                                                      IntSummaryStatistics::combine);

        System.out.printf(Locale.ENGLISH, "+ Co-authors by author:%n");
        System.out.printf(Locale.ENGLISH, "  - maximum number: %,d%n", coauthorsSummary.getMax());
        System.out.printf(Locale.ENGLISH, "  - average: %.2f%n", coauthorsSummary.getAverage());
        System.out.printf(Locale.ENGLISH, "  - minimum number: %,d%n", coauthorsSummary.getMin());
        System.out.printf(Locale.ENGLISH, "%n");


        System.out.printf(Locale.ENGLISH, "+ Authors with max co-authors%n");
        authors.values().stream().filter(sts -> sts.coauthors.size() == coauthorsSummary.getMax()).forEach(
                sts -> System.out.printf(Locale.ENGLISH, "  - %s%n", sts.name));
        System.out.printf(Locale.ENGLISH, "%n");


        final IntSummaryStatistics paperSummary = authors.values().stream().mapToInt(s -> s.papers).collect(
                IntSummaryStatistics::new, IntSummaryStatistics::accept, IntSummaryStatistics::combine);

        System.out.printf(Locale.ENGLISH, "+ Papers by author:%n");
        System.out.printf(Locale.ENGLISH, "  - maximum number: %,d%n", paperSummary.getMax());
        System.out.printf(Locale.ENGLISH, "  - average: %.2f%n", paperSummary.getAverage());
        System.out.printf(Locale.ENGLISH, "  - minimum number: %,d%n", paperSummary.getMin());
        System.out.printf(Locale.ENGLISH, "%n");

        System.out.printf(Locale.ENGLISH, "+ Authors with max papers%n");
        authors.values().stream().filter(sts -> sts.papers == paperSummary.getMax()).forEach(
                sts -> System.out.printf(Locale.ENGLISH, "  - %s%n", sts.name));
        System.out.printf(Locale.ENGLISH, "%n");


        final IntSummaryStatistics fullPaperSummary = authors.values().stream().mapToInt(s -> s.fullPapers).collect(
                IntSummaryStatistics::new, IntSummaryStatistics::accept, IntSummaryStatistics::combine);

        System.out.printf(Locale.ENGLISH, "+ Full papers by author:%n");
        System.out.printf(Locale.ENGLISH, "  - maximum number: %,d%n", fullPaperSummary.getMax());
        System.out.printf(Locale.ENGLISH, "  - average: %.2f%n", fullPaperSummary.getAverage());
        System.out.printf(Locale.ENGLISH, "  - minimum number: %,d%n", fullPaperSummary.getMin());
        System.out.printf(Locale.ENGLISH, "%n");

        System.out.printf(Locale.ENGLISH, "+ Authors with max full papers%n");
        authors.values().stream().filter(sts -> sts.fullPapers == fullPaperSummary.getMax()).forEach(
                sts -> System.out.printf(Locale.ENGLISH, "  - %s%n", sts.name));
        System.out.printf(Locale.ENGLISH, "%n");


        final IntSummaryStatistics shortPaperSummary = authors.values().stream().mapToInt(s -> s.shortPapers).collect(
                IntSummaryStatistics::new, IntSummaryStatistics::accept, IntSummaryStatistics::combine);

        System.out.printf(Locale.ENGLISH, "+ Short papers by author:%n");
        System.out.printf(Locale.ENGLISH, "  - maximum number: %,d%n", shortPaperSummary.getMax());
        System.out.printf(Locale.ENGLISH, "  - average: %.2f%n", shortPaperSummary.getAverage());
        System.out.printf(Locale.ENGLISH, "  - minimum number: %,2d%n", shortPaperSummary.getMin());
        System.out.printf(Locale.ENGLISH, "%n");

        System.out.printf(Locale.ENGLISH, "+ Authors with max short papers%n");
        authors.values().stream().filter(sts -> sts.shortPapers == shortPaperSummary.getMax()).forEach(
                sts -> System.out.printf(Locale.ENGLISH, "  - %s%n", sts.name));
        System.out.printf(Locale.ENGLISH, "%n");


        final IntSummaryStatistics pageSummary = authors.values().stream().mapToInt(s -> s.pages).collect(
                IntSummaryStatistics::new, IntSummaryStatistics::accept, IntSummaryStatistics::combine);

        System.out.printf(Locale.ENGLISH, "+ Pages by author:%n");
        System.out.printf(Locale.ENGLISH, "  - maximum number: %,d%n", pageSummary.getMax());
        System.out.printf(Locale.ENGLISH, "  - average: %.2f%n", pageSummary.getAverage());
        System.out.printf(Locale.ENGLISH, "  - minimum number: %,d%n", pageSummary.getMin());
        System.out.printf(Locale.ENGLISH, "%n");

        System.out.printf(Locale.ENGLISH, "+ Authors with max pages%n");
        authors.values().stream().filter(sts -> sts.pages == pageSummary.getMax()).forEach(
                sts -> System.out.printf(Locale.ENGLISH, "  - %s%n", sts.name));
        System.out.printf(Locale.ENGLISH, "%n");


        System.out.printf(Locale.ENGLISH, "+ Paper with the longest title: %,d characters%n",
                          longestTitle.title.length());
        System.out.printf(Locale.ENGLISH, "  - title: %s%n", longestTitle.title);
        System.out.printf(Locale.ENGLISH, "  - year: %s%n", longestTitle.year);
        System.out.printf(Locale.ENGLISH, "  - authors: %,d%n     ", longestTitle.authors.size());
        for (String author : longestTitle.authors) {
            System.out.printf(Locale.ENGLISH, "%s; ", author);
        }
        System.out.printf(Locale.ENGLISH, "%n%n");


        System.out.printf(Locale.ENGLISH, "+ Paper with the shortest title: %,d characters%n",
                          shortestTitle.title.length());
        System.out.printf(Locale.ENGLISH, "  - title: %s%n", shortestTitle.title);
        System.out.printf(Locale.ENGLISH, "  - year: %s%n", shortestTitle.year);
        System.out.printf(Locale.ENGLISH, "  - authors: %,d%n     ", shortestTitle.authors.size());
        for (String author : shortestTitle.authors) {
            System.out.printf(Locale.ENGLISH, "%s; ", author);
        }
        System.out.printf(Locale.ENGLISH, "%n");

        System.out.printf(Locale.ENGLISH, "+ Paper with the maximum number of authors:%n");
        System.out.printf(Locale.ENGLISH, "  - title: %s%n", maxAuthors.title);
        System.out.printf(Locale.ENGLISH, "  - year: %s%n", maxAuthors.year);
        System.out.printf(Locale.ENGLISH, "  - authors: %,d%n     ", maxAuthors.authors.size());
        for (String author : maxAuthors.authors) {
            System.out.printf(Locale.ENGLISH, "%s; ", author);
        }
        System.out.printf(Locale.ENGLISH, "%n%n");


        int maxIntersection = Integer.MIN_VALUE;
        Set<String> tmp;
        List<String[][]> maxInter = new ArrayList<>();
        String[][] dummy;


        // only the upper triangle would be enough but scanning the full matrix is a bit simpler
        for (AuthorStats author1 : authors.values()) {

            for (AuthorStats author2 : authors.values()) {

                if (!author1.equals(author2)) {

                    tmp = new TreeSet<>(author1.coauthors.keySet());

                    tmp.retainAll(author2.coauthors.keySet());

                    if (tmp.size() == maxIntersection) {
                        dummy = new String[1][2];
                        dummy[0][0] = author1.name;
                        dummy[0][1] = author2.name;

                        maxInter.add(dummy);
                    } else if (tmp.size() > maxIntersection) {
                        maxIntersection = tmp.size();

                        maxInter.clear();

                        dummy = new String[1][2];
                        dummy[0][0] = author1.name;
                        dummy[0][1] = author2.name;

                        maxInter.add(dummy);
                    }

                }

            }

        }


        System.out.printf(Locale.ENGLISH, "+ Authors with the largest number of common co-authors: %,d%n",
                          maxIntersection);
        for (String[][] pair : maxInter) {
            System.out.printf(Locale.ENGLISH, "  - %s and %s%n", pair[0][0], pair[0][1]);
        }
        System.out.printf(Locale.ENGLISH, "%n");

    }


    /**
     * Aligns the XML stream reader at the start of the specified element.
     *
     * @param localName the name of the element.
     *
     * @return {@code true} if the start of the element {@code localName} has been found; {@code false} otherwise.
     *
     * @throws IllegalStateException if something goes wrong while parsing the XML.
     */
    private final boolean alignAtElementStart(final String localName) throws IllegalStateException {


        if (localName == null) {
            LOGGER.error("Local name cannot be null.");
            throw new NullPointerException("Local name cannot be null.");
        }

        if (localName.isEmpty()) {
            LOGGER.error("Local name cannot be empty.");
            throw new IllegalStateException("Local name cannot be empty.");
        }


        try {
            // while we are not on the start of an element or the element is not
            // a token element, advance to the next element (if any)
            while (xsr.getEventType() != XMLStreamConstants.START_ELEMENT || !localName.equals(xsr.getLocalName())) {

                // there are no more events
                if (!xsr.hasNext()) {
                    return false;
                }

                xsr.next();
            }
        } catch (final XMLStreamException e) {
            Message msg = new ParameterizedMessage(
                    "Unable to parse the XML document and align stream to the start of element {}.", localName);
            LOGGER.error(msg, e);
            throw new IllegalStateException(msg.getFormattedMessage(), e);
        }

        return true;
    }


    public static void main(String[] args) {


        DblpParser p = new DblpParser();

        p.parse();

        p.printStats();

    }


}
