/*
 *  Copyright 2017 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.ims.sigir40;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.ParameterizedMessage;

import java.util.HashSet;
import java.util.Set;

/**
 * Represents a DBLP record.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
class Record implements Comparable<Record> {

    /**
     * The logger
     */
    private static final Logger LOGGER = LogManager.getLogger(Record.class);

    /**
     * The start year of the publications under consideration
     */
    private static final int START_YEAR = 1978;

    /**
     * The start year of the publications under consideration
     */
    private static final int END_YEAR = 2017;

    /**
     * The DBLP key of a publication
     */
    final String key;

    /**
     * The DOI of a publication
     */
    final String doi;

    /**
     * The title of a publication
     */
    final String title;

    /**
     * The year of a publication
     */
    final int year;

    /**
     * The page range of a publication
     */
    final String pages;

    /**
     * The length of a publication
     */
    final int length;

    /**
     * Indicates whether it is a full paper ({@code true}) or a short paper/poster/demo ({@code false})
     */
    final boolean fullPaper;

    /**
     * The authors of a publication
     */
    final Set<String> authors = new HashSet<>();


    /**
     * Creates a new DBLP record.
     *
     * @param key       the DBLP key of a publication.
     * @param doi       the DOI of a publication.
     * @param title     the title of a publication.
     * @param year      the title of a publication.
     * @param pages     the page range a publication.
     * @param length    the length of a publication.
     * @param fullPaper indicates whether it is a full paper ({@code true}) or a short paper/poster/demo ({@code
     *                  false}).
     */
    public Record(final String key, final String doi, final String title, final int year, final String pages,
                  final int length, final boolean fullPaper) {

        if (key == null) {
            LOGGER.error("Record key cannot be null.");
            throw new NullPointerException("Record key cannot be null.");
        }

        if (key.isEmpty()) {
            LOGGER.error("Record key cannot be empty.");
            throw new IllegalArgumentException("Record key cannot be empty.");
        }
        this.key = key;

        if (doi == null) {
            Message msg = new ParameterizedMessage("Record DOI is null for paper with key {}.", key);
            LOGGER.warn(msg);
        }

        if (doi != null && doi.isEmpty()) {
            Message msg = new ParameterizedMessage("Record DOI is empty for paper with key {}.", key);
            LOGGER.warn(msg);
        }
        this.doi = doi;

        if (title == null) {
            Message msg = new ParameterizedMessage("Record title cannot be null for paper {}.", key);
            LOGGER.error(msg);
            throw new NullPointerException(msg.getFormattedMessage());
        }

        if (title.isEmpty()) {
            Message msg = new ParameterizedMessage("Record title cannot be empty for paper {}.", key);
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }
        this.title = title;

        if (year < START_YEAR || year > END_YEAR) {
            Message msg = new ParameterizedMessage(
                    "Publication year is {} for paper {}; expected to be in the range {}-{}.", year, key, START_YEAR,
                    END_YEAR);
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }
        this.year = year;

        if (pages == null) {
            Message msg = new ParameterizedMessage("Record page range cannot be null for paper {}.", key);
            LOGGER.error(msg);
            throw new NullPointerException(msg.getFormattedMessage());
        }

        if (pages.isEmpty()) {
            Message msg = new ParameterizedMessage("Record page range cannot be empty for paper {}.", key);
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }
        this.pages = pages;

        if (length < 1) {
            Message msg = new ParameterizedMessage(
                    "Length is {} for paper {}; expected to be greater than or equal to 1.", year, key);
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }
        this.length = length;

        this.fullPaper = fullPaper;
    }

    @Override
    public boolean equals(final Object o) {
        return (this == o) || (o instanceof Record && key.equals(((Record) o).key));
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }

    @Override
    public int compareTo(Record o) {
        return o == null ? 1 : key.compareTo(o.key);
    }

}
